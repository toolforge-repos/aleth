#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#	   sem imagd.py
#
#	   (C) 2012 - 2020 Alchimista <alchimistawp@gmail.com>
#
#		Distributed under the terms of the GNU GPL license.


import pywikibot
from pywikibot import pagegenerators
import re
import datetime as dtime
from pywikibot import date as dt
import mwparserfromhell
from pywikibot.data import api


# ===========================================================================
#
# TODO: cleanup code
#
# ===========================================================================

##Maximum days where the bot will try to add images
# max = 18
#
## Date variables
# now = dtime.datetime.utcnow()
##print now
# nowDay = dtime.date.today().strftime("%d")
# nowMonth = str(abs(int((dtime.date.today().strftime("%m")))))
# nowYear = dtime.date.today().strftime("%Y")


def wikiPage(day, month, year):
    monthpt = dt.formats['MonthName']['pt'](abs(int(month))).lower()
    page = u"Wikipédia:Imagem em destaque/%s de %s de %s" % (abs(int(day)), monthpt, year)
    print ("\n_______\n", day, month,year, page)
    return page


def commonsPage(day, month, year):
    page = u"Template:Potd/%s-%s-%s" % (year, month, day)
    return page


def getImageSize(self):
    """Return the URL for the image described on this page."""
    # TODO add scaling option?
    # if not hasattr(self, '_imagesize'):
    #     self._imageinfo = self.site.loadimageinfo(self)
    #     print ("----" , self._imageinfo)
    # self._imageinfo = self.site.loadimageinfo(self)
    # print("----", self._imageinfo)
    # return self._imageinfo['width'], self._imageinfo['height']
    print (self.getFileVersionHistory(self))


def main():
    site = pywikibot.Site("pt", "wikipedia")
    site = pywikibot.Site("pt", "wikipedia")
    commonsSite = pywikibot.Site('commons', 'commons')

    #	# Date variables
    now = dtime.datetime.utcnow()
    #	#print now
    #	nowDay = dtime.date.today().strftime("%d")
    #	nowMonth = str(abs(int((dtime.date.today().strftime("%m")))))
    #	nowYear = dtime.date.today().strftime("%Y")
    #	#Maximum days where the bot will try to add images
    maxt = 40
    # date = now
    t = 0
    #	print wikiPage(nowDay,nowMonth,nowYear)
    #	print commonsPage(nowDay,nowMonth,nowYear)

    while t < (maxt + 1):
        s = dtime.timedelta(t)
        day = (now + s).strftime("%d")
        month = (now + s).strftime("%m")
        year = (now + s).strftime("%Y")

        wpagetitle = wikiPage(day, month, year)
        cpagetitle = commonsPage(day, month, year)
        linkcpagetitle = u"commons:%s" % cpagetitle
        print (day,month,year, wpagetitle,cpagetitle,linkcpagetitle)

        sp = pywikibot.Page(site, wpagetitle)
        if not sp.exists():
            '''Search for non existent pages '''

            pywikibot.output(u"\n A página não existe: %s" % wpagetitle)

            cpagetitlept = cpagetitle
            cpagetitlept += u" (pt)"
            cpagetitleen = u"%s (en)" % cpagetitle
            imag = []
            template = []
            try:
                ''' First will try to see if there is a page in commons
                with the image of the day'''

                cimag = pywikibot.Page(commonsSite, cpagetitle)
                centext = cimag.get()
                pywikibot.output(centext)
                wikicode = mwparserfromhell.parse(centext)

                imag = wikicode.filter_templates()[0].get(1).value.split("<!--")[0]
                print (imag)


                # imagr = re.compile(
                #     r"\{\{Potd\s filename\|(?:\d{1}\=)?(.*)\|(?:\d{1}\=)?\d{4}\|(?:\d{1}\=)?\d{1,2}\|(?:\d{1,2}\=)?\d{1,2}\}\}",
                #     (re.I | re.S | re.X | re.U))
                # imag = re.findall(imagr, centext)
                # pywikibot.output(imag)


                try:
                    '''Now wi'll try to get pt description'''
                    cimag2 = pywikibot.Page(commonsSite, cpagetitlept)
                    centext2 = cimag2.get()
                    descr = re.compile(r"\{\{Potd description\|(?:1\=)?(.*)\|(?:2\=\w*)", re.I)
                    desc = re.findall(descr, centext2)
                    if desc != []:
                        desc = re.findall(descr, centext2)[0]
                    lang = u"pt"
                #					print desc
                #					print centext2

                except pywikibot.exceptions.NoPageError:
                    ''' If pt description doesn't exists, lets search en'''

                    cimag2 = pywikibot.Page(commonsSite, cpagetitleen)
                    centext2 = cimag2.get()
                    lang = u"en"
                    descr = re.compile(r"\{\{Potd description\|(?:1\=)?(.*)\|(?:2\=\w*)", re.I)
                    desc = re.findall(descr, centext2)
                    if desc != []:
                        desc = re.findall(descr, centext2)[0]
                    elif desc == [] and centext2 != []:
                        desc = centext2
                #					print centext2.title()
                #					print desc
                #					print centext2

                pywikibot.output(u"imagem: %s :: %s :: %s" % (imag, type(cimag), imag))
                #imag = imag.search("<!--DON'T EDIT BELOW THIS LINE.")
                print ("- img:",imag, "\n::")
                cimagInfo = pywikibot.Page(commonsSite, title=u"File:%s" % imag)
                print ("\n--\nimag:\n", cimagInfo, "\n:::")
                pywikibot.output(cimagInfo)
                print (pywikibot.FilePage(cimagInfo))
                #size = getImageSize(pywikibot.FilePage(cimagInfo))
                f_data = pywikibot.FilePage(cimagInfo).latest_file_info
                #print ("????, ", type(size._file_revisions), size.latest_file_info)
                size = f_data['width'] , f_data['height']

                pywikibot.output(u"texto e imagem: %s :: %s :: %s:: %s" % (imag, lang, desc, centext2))

                if imag != []:
                    # 					template = u'{{Wikipedia:Imagem em destaque/modelo3'
                    # 					template += u'\n| orientação = <!-- coloque "H" se a imagem estiver na horizontal ou "V" na vertical -->'
                    # 					template += u'\n<!-- Imagens na vertical costumam estragar o layout da página principal, evite-as sempre que possível -->'
                    # 					template += u'\n| imagem	 = %s' % imag
                    template = u"{{Imagem do dia|%s|l=%s|h=%s" % (imag, size[0], size[1])
                    pywikibot.output(template)

                    if lang == u"en" and desc != []:
                        template += u"\n| legenda_en = %s\n|<!-- Descrição da imagem -->" % desc
                        template += u"\n|min={{{min|{{Predefinição:Imagem do dia/min}}}}}|max={{{max|{{Predefinição:Imagem do dia/max}}}}}}}\n<noinclude>[[Categoria:!Imagens em destaque de %s de %s]]</noinclude>" % (
                            dt.formats['MonthName']['pt'](abs(int(month))).lower(), year)
                    elif lang == u"pt" and desc != []:
                        template += u"\n|  %s" % desc
                        template += u"\n|min={{{min|{{Predefinição:Imagem do dia/min}}}}}|max={{{max|{{Predefinição:Imagem do dia/max}}}}}}}\n<noinclude>[[Categoria:!Imagens em destaque de %s de %s]]</noinclude>" % (
                            dt.formats['MonthName']['pt'](abs(int(month))).lower(), year)

                    pywikibot.output(template)

                    if template != [] and template != "":
                        pywikibot.output(u"Finally it's time to save!!!")
                        comment = u"[[wp:BOT|BOT]]: A importar [[%s|imagem de destaque]] do commons." % linkcpagetitle
                        sp.text = template
                        sp.save(minorEdit=False, summary=comment)

            except pywikibot.exceptions.NoPageError:
                pywikibot.output(u"a página não existe: %s" % cpagetitle)
                pass
        else:
            pywikibot.output(u"página existe: %s " % sp.title())
            pass
        #			text = sp.get()
        #			print wpagetitle
        #			print text
        # print (now + s).strftime("%d-%m-%Y")
        t = t + 1


if __name__ == "__main__":
    try:
        main()
    finally:
        pywikibot.stopme()
